#!/usr/bin/env python

import time
import webapp2
import gdata

class welcome(webapp2.RequestHandler):
    def get(self):
        self.response.write('Welcome to Timeclock!\n\nPlease clock-in or clock-out.')

class clockin(webapp2.RequestHandler):
    def get(self):
        clockintime = self.request.get("time")
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, webapp2 World!\nClock-in Time: %s' % time.ctime(float(clockintime or time.time())))

class clockout(webapp2.RequestHandler):
    pass

app = webapp2.WSGIApplication([('/clockin', clockin), ('/clockout',clockout),('/',welcome)],
                              debug=True)
